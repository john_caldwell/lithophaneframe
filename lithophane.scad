/* [Lithophane Settings] */
// surface height in mm
height = 2.2;

// mirror the surface 1 for true 0 for false
mirrorImage = true;

mirrorValue = mirrorImage ? 1 : 0;

// rotate the image around the z axis
imageRotation = 0;

// base layer in mm
baseLayer = 0.8;

// circum-diameter of lithopahne in inches
in = 6.1;

// File Path
fileName = "";

/* [Frame Settings] */
// Number of sides for the frame
numberOfSides = 6; // [6:2:14]

// Frame height in inchs
frameHeightIn = 1.5;

// The "lip" of the frame for the lithophane to fit into
innerFrameWidth = 2;

// The width of the frame that extends beyond the lithophane
frameWidth = 1;

// An array with numbers form 0 to 5 to have holes to pass wires through
facesWithHoles = [];

// Mount Clip Length
mountClipLength = 30.0;

// How Big the wire hole should be
wireRadius = 7.5;

// How many angles for the wire hole
wireHoleAngles = 4;

// Space between wire clip and wall
wireClipGap = 2;

// Wire clip thickness
wireClipThickness = 2;

// Width of each wire clip;
wireClipWidth = 10;

/* [Render Settings] */
// Render the lithophane
renderSurface = false;

// Render the frame
renderFrame = true;

// Should the sample surface render?
renderSampleSurface = true;

// Render the wire hole plug
renderHolePlug = false;

// Render the wire hole plug on side 4
renderSampleHolePlug = false;

// Show a cross section to see fit
cutCrossSection = true;

// horizontal error during extrusion
horizontalMargin = 0.25;

// vertical margin between parts
verticalMargin = 0.1;

exteriorAngle = 360 / numberOfSides;
interiorAngle = 180 - exteriorAngle;

totalFrameWidth = innerFrameWidth + frameWidth;

frameHeight = frameHeightIn * 25.4;
surfaceHeight = height + baseLayer;
diameter = in * 25.4;
lithophaneRadius = diameter / 2;
mountHeight = min(totalFrameWidth, 5);

innerRingRadius = lithophaneRadius + (horizontalMargin - innerFrameWidth) / sin(interiorAngle / 2);
outerRingRadius = innerRingRadius + totalFrameWidth / sin(interiorAngle / 2);

frameApothem = innerRingRadius * cos(180/numberOfSides);
sideLength = 2 * innerRingRadius * sin(180/numberOfSides);

module frameRing() {
    $fn = numberOfSides;

    shelfRadius = outerRingRadius - frameWidth / sin(exteriorAngle);

    color("blue") difference() {
        cylinder(h = frameHeight, r = outerRingRadius, center = true);
        cylinder(h = frameHeight, r = innerRingRadius, center = true);

        translate([0, 0, (frameHeight - surfaceHeight) / 2])
            cylinder(h = surfaceHeight + verticalMargin, r = shelfRadius, center = true);
    }
}

module wireHoles() {
    for (face = facesWithHoles) {
        rotate([0, 0, face * exteriorAngle + exteriorAngle / 2])
                translate([frameApothem - 5, 0, 0])
                    rotate([0, 90, 0])
                        cylinder(h = totalFrameWidth + 10, r = wireRadius, $fn = wireHoleAngles);
    }
}

module wireHolePlug() {
  _exteriorAngle = 360 / wireHoleAngles;
  _interiorAngle = 180 - _exteriorAngle;
  _horizontalMargin = horizontalMargin / sin(_interiorAngle / 2);
  _verticalMargin = verticalMargin / sin(_interiorAngle / 2);

  cylinder(h = totalFrameWidth + _horizontalMargin * 2, r = wireRadius - _verticalMargin * 2, $fn = wireHoleAngles, center = true);

  translate([0, 0, - 0.5 - (totalFrameWidth + _verticalMargin) / 2 ])
   cylinder(h = 1, r = wireRadius - _horizontalMargin + 0.75, $fn = wireHoleAngles, center = true);
}

module wireClips() {
    clipHeight = (frameHeight - surfaceHeight - verticalMargin) * .95;
    totalClipThickness = wireClipThickness + wireClipGap;
    totalClipLength = sideLength * .9;
    clipGap = totalClipLength - 2 * wireClipWidth;

    color("orange") difference() {
        for(face = [0:(numberOfSides - 1)]) {
            rotate([0, 0, face * exteriorAngle + exteriorAngle / 2])
                translate([frameApothem - totalClipThickness, -totalClipLength / 2, -frameHeight / 2]) {
                    difference() {
                        cube([totalClipThickness, totalClipLength, clipHeight]);

                        translate([wireClipThickness, 0, wireClipThickness])
                            cube([wireClipGap, totalClipLength, clipHeight]);

                        translate([0, (totalClipLength - clipGap) / 2, 0])
                            cube([totalClipThickness, clipGap, clipHeight]);
                    }
                }
        }
    }
}

module wallMount() {
    $fn = 60;
    mountScrewHoleLength = mountClipLength / 2;
    mountScrewHoleTranlate = mountScrewHoleLength / 2;
    bigRadius = mountClipLength / 8;
    littleRadius = mountClipLength / 16;
    mountWidth = mountClipLength * 2 / 3;

    difference() {
        cube([mountWidth, mountClipLength, mountHeight], center = true);
        cylinder(h = mountHeight, r = bigRadius, center = true);
        translate([0, -mountScrewHoleTranlate, 0])
            cylinder(h = mountHeight, r = littleRadius, center = true);
        translate([0, mountScrewHoleTranlate, 0])
            cylinder(h = mountHeight, r = littleRadius, center = true);
        cube([littleRadius * 2, mountScrewHoleLength, mountHeight], center = true);
    }
}

module frame() {
    difference() {
        frameRing();
        wireHoles();
    }

    wallMountTranslation = frameApothem - mountClipLength / 2;
    translate([0, wallMountTranslation, -(frameHeight / 2) + mountHeight / 2])
        wallMount();
    translate([0, -wallMountTranslation, -(frameHeight / 2) + mountHeight / 2])
        wallMount();

    wireClips();
}

module surfaceLayer() {
    intersection() {
        cylinder(h = height + 5, r = lithophaneRadius, center = true, $fn = numberOfSides);

        rotate([0, 0, imageRotation])
          resize([diameter, diameter, height])
            mirror([mirrorValue, 0, 0])
              surface(fileName, center = true, invert = true);
    }
}

module baseLayer() {
    halfBaseHeight = baseLayer / 2;
    baseLayerZChange = -1 * ( height + halfBaseHeight );

    translate([0, 0, baseLayerZChange])
    cylinder(h = baseLayer, r = lithophaneRadius, center = true, $fn = numberOfSides);
}

difference() {
  union() {
    if(renderSurface) {
      color("white") {
        surfaceLayer();
        baseLayer();
      }
    }

    if(renderFrame) {
        frame();
    }

    if (renderSampleSurface) {
      lithophaneHeight = baseLayer + height;

      color("white") translate([0, 0, frameHeight / 2 - lithophaneHeight])
        cylinder(h = baseLayer + height, r = lithophaneRadius, $fn = numberOfSides);
    }

    if(renderSampleHolePlug) {
        translate([0, -frameApothem - totalFrameWidth / 2, 0])
          rotate([90, 90, 0])
            color("red") wireHolePlug();
    }
  }

  if (cutCrossSection) {
    rotate([0, 90, 0])
    translate([-diameter / 2, 0, -diameter / 4])
      cube([diameter * 2, diameter, frameHeight * 2 + 5], center=true);
  }
}

if (renderHolePlug) {
  wireHolePlug();
}