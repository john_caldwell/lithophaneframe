#ifndef LOGGING_h
#define LOGGING_h

#define SETUP_LOGGER                                  \
  Serial.begin(115200);

#define LOG_MESSAGE(format, ...)                      \
  {                                                   \
    char buffer[1024];                                \
    sprintf(buffer, format, ##__VA_ARGS__);           \
    Serial.println(buffer);                           \
  }

#define LOG_IF(condition, format, ...)                \
  if (condition) {                                    \
    LOG_DEBUG_MESSAGE(format, ##__VA_ARGS__);         \
  }

#ifdef DEBUG_MODE
#define SETUP_DEBUG_LOGGER SETUP_LOGGER
#define LOG_DEBUG_MESSAGE(format, ...) LOG_MESSAGE(format, ##__VA_ARGS__)
#define LOG_DEBUG_IF(condition, format, ...) LOG_IF(condition, format, ##__VA_ARGS__)
#else
#define SETUP_DEBUG_LOGGER do { (void)0; } while(0)
#define LOG_DEBUG_MESSAGE(...) do { (void)0; } while(0)
#define LOG_DEBUG_IF(...) do { (void)0; } while(0)
#endif

#endif
