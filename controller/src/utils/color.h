#ifndef COLOR_h
#define COLOR_h

#include <Arduino.h>

typedef struct Color {
  byte red;
  byte green;
  byte blue;

  Color() {};

  Color(byte redIntensity, byte greenIntensity, byte blueIntensity) {
    initialize(redIntensity, greenIntensity, blueIntensity);
  }

  Color(uint32_t color) {
    byte redPart = (color & 0xFF0000) >> 16;
    byte greenPart = (color & 0xFF00) >> 8;
    byte bluePart = (color & 0xFF);

    initialize(redPart, greenPart, bluePart);
  }

  Color adjustColorIntensity(double percent) {
    byte adjustedRed = red * percent;
    byte adjustedGreen = green * percent;
    byte adjustedBlue = blue * percent;

    return Color(adjustedRed, adjustedGreen, adjustedBlue);
  }

  const Color interpolate(const Color next, const double progress) const {
    byte adjustedRed = interpolate(red, next.red, progress);
    byte adjustedGreen = interpolate(green, next.green, progress);
    byte adjustedBlue = interpolate(blue, next.blue, progress);

    return Color(adjustedRed, adjustedGreen, adjustedBlue);
  }

  operator const uint32_t() const {
    return ((uint32_t)red << 16) | ((uint32_t)green << 8) | (uint32_t)blue;
  }

  private:
    byte interpolate(byte current, byte next, double progress) const {
      return (next - current) * progress + current;
    }

    void initialize(byte redIntensity, byte greenIntensity, byte blueIntensity) {
      red = redIntensity;
      green = greenIntensity;
      blue = blueIntensity;
    }
} Color;

#endif