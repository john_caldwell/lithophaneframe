#include "twinkle.h"

void Twinkle::animate() {
  byte randomGroup = random(groups);
  byte startingLed = randomGroup * groupSize;

  strip.clear();
  fillStrip(color, startingLed, groupSize);
  strip.show();

  delay(duration);
}