#ifndef ANIMATION_h
#define ANIMATION_h

#include <Arduino.h>
#include <Adafruit_NeoPixel.h>

#include "../utils.h"

class Animation {
  public:
    Animation(Adafruit_NeoPixel strip, byte groupSize) : strip(strip), groupSize(groupSize) {
      groups = strip.numPixels() / groupSize;
    }
    virtual void animate() = 0;

  protected:
    Adafruit_NeoPixel strip;
    byte groupSize;
    byte groups;

    void fillStrip(Color color, uint16_t first, uint16_t count) {
      const auto gammaRed = pgm_read_byte(&gammaR[color.red]);
      const auto gammaGreen = pgm_read_byte(&gammaG[color.green]);
      const auto gammaBlue = pgm_read_byte(&gammaB[color.blue]);

      Color gammaColor(gammaRed, gammaGreen, gammaBlue);
      strip.fill(gammaColor, first, count);
    }

    void setPixelColor(uint16_t pixel, Color color) {
      fillStrip(color, pixel, 1);
    }
};

#endif