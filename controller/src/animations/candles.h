#ifndef CANDLES_h
#define CANDLES_h

#include "animation.h"

class Candles : public Animation {
  public:
    Candles(Adafruit_NeoPixel strip, int groupSize, Color flameColor = 0xFDA42C, double wind = .2) :
      Animation(strip, groupSize),
      flameColor(flameColor),
      wind(wind) { };

    void animate();

  private:
    const Color flameColor;
    const double wind;

    unsigned long lastUpdateTime;
    double candles[100];

    void blowCandle(byte candleIndex);
    void showCandle(byte candleIndex);
};

#endif