#ifndef CROSSFADE_h
#define CROSSFADE_h

#include "animation.h"
#include "../utils.h"

class Crossfade: public Animation {
  public:
    Crossfade(
      Adafruit_NeoPixel strip,
      int groupSize,
      const Color colors[],
      const byte colorCount,
      const unsigned int delay = 5 SECONDS,
      const unsigned int duration = 1 SECONDS
    ):
      Animation(strip, groupSize),
      colors(colors),
      colorCount(colorCount),
      delay(delay),
      duration(duration)
    { };

    void animate();

  private:
    const unsigned int INCREMENTS = 1000;

    const Color *colors;
    const byte colorCount;
    const unsigned long delay;
    const unsigned long duration;

    Color currentColors[100];

    unsigned long lastTransition = 0;

    const Color pickNextColor(const byte groupIndex);
    void transitionToColor(const byte groupIndex, const Color color);
    void initializeColors();
};

#endif