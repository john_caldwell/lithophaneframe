#include "spinners.h"
#include "../logging.h"

Spinners::Spinners(Adafruit_NeoPixel strip, int groupSize, uint32_t rpm, const Color colors[], const byte colorCount) :
  Animation(strip, groupSize),
  rpm(rpm),
  colors(colors),
  colorCount(colorCount)
{ }

void Spinners::animate() {
  int revolutionDuration = 60000 / rpm;
  double cyclePosition = 1.0 - (millis() % revolutionDuration) * 1.0 / revolutionDuration;

  for (int i = 0; i < groupSize; ++i) {
    const double ledPositionOffset = 1.0 * i / groupSize;
    double positionInColors = (cyclePosition + ledPositionOffset) * colorCount;

    while (positionInColors > colorCount) {
      positionInColors -= colorCount;
    }

    const int currentLedIndex = (int)floor(positionInColors) % colorCount;
    const int nextLedIndex = (currentLedIndex + 1) % colorCount;
    const double interpolation = positionInColors - currentLedIndex;

    const auto currentColor = colors[currentLedIndex];
    const auto nextColor = colors[nextLedIndex];

    auto interpolatedColor = currentColor.interpolate(nextColor, interpolation);

    for (int j = 0; j < groups; ++j) {
      setPixelColor(j * groupSize + i, interpolatedColor);
    }
  }

  strip.show();
}