#ifndef TWINKLE_h
#define TWINKLE_h

#include "animation.h"

class Twinkle : public Animation {
  public:
    Twinkle::Twinkle(Adafruit_NeoPixel strip, int groupSize, uint32_t twinkleDuration, Color color = 0xFFFFFF) :
      Animation(strip, groupSize),
      duration(twinkleDuration),
      color(color)
    { };

    void animate();

  private:
    const Color color;
    const uint32_t duration;
};

#endif