#ifndef SPINNERS_h
#define SPINNERS_h

#include "animation.h"

class Spinners : public Animation {
  public:
    Spinners(Adafruit_NeoPixel strip, int groupSize, uint32_t rpm, const Color colors[], const byte colorCount);
    void animate();

  private:
    const Color *colors;
    const byte colorCount;

    const uint32_t rpm;
};

#endif