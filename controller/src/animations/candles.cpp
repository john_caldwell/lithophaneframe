#include "candles.h"

void Candles::animate() {
  if (millis() - lastUpdateTime < 40) { return; }

  for (int i = 0; i < groups; ++i) {
    blowCandle(i);
    showCandle(i);
  }

  strip.show();
  lastUpdateTime = millis();
}

void Candles::blowCandle(byte candleIndex) {
  float heatIncrease = random(50, 150) / 1000.0;

  double currentHeat = candles[candleIndex];
  currentHeat += heatIncrease;

  double tumble = random(0, 1000) / 1000.0;
  tumble = pow(tumble, 2);
  tumble *= wind * 1.5;
  currentHeat *= (1.0 - tumble);

  currentHeat = constrain(currentHeat, 0.0, 1.0);
  candles[candleIndex] = currentHeat;
}

void Candles::showCandle(byte candleIndex) {
  double currentHeat = candles[candleIndex];
  auto currentColor = Color(0x000000).interpolate(flameColor, currentHeat);

  uint16_t startingLed = candleIndex * groupSize;
  fillStrip(currentColor, startingLed, groupSize);
}