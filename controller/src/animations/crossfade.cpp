#include "crossfade.h"

void Crossfade::initializeColors() {
  for (byte i = 0; i < groups; ++i) {
    currentColors[i] = pickNextColor(i);
    const byte startingLed = i * groupSize;
  }

  lastTransition = millis();
}

void Crossfade::animate() {
  if (lastTransition == 0) {
    initializeColors();
  }

  if (millis() - lastTransition < delay) {
    for (byte i = 0; i < groups; ++i) {
      const byte startingLed = i * groupSize;
      const auto groupColor = currentColors[i];
      fillStrip(groupColor, startingLed, groupSize);
    }

    strip.show();
    return;
  }

  const byte randomGroup = random(groups);
  const auto nextColor = pickNextColor(randomGroup);

  transitionToColor(randomGroup, nextColor);
  lastTransition = millis();
}

const Color Crossfade::pickNextColor(const byte groupIndex) {
  const auto currentColor = currentColors[groupIndex];

  Color nextColor;

  do {
    const byte nextIndex = random(colorCount);
    nextColor = colors[nextIndex];
  } while (nextColor == currentColor);

  return nextColor;
}

void Crossfade::transitionToColor(const byte groupIndex, const Color nextColor) {
  const unsigned long incrementDelay = delay * 1000 / INCREMENTS;
  const auto currentColor = currentColors[groupIndex];

  const byte startingLed = groupIndex * groupSize;

  for (unsigned int i = 0; i < INCREMENTS; ++i) {
    const double progress = i * 1.0 / INCREMENTS;
    const auto intermediateColor = currentColor.interpolate(nextColor, progress);
    fillStrip(intermediateColor, startingLed, groupSize);
    strip.show();

    delayMicroseconds(incrementDelay);
  }

  currentColors[groupIndex] = nextColor;
}