#ifndef UTILITIES_h
#define UTILITIES_h

#include "utils/time.h"
#include "utils/color.h"
#include "utils/strip_helpers.h"

#define ARRAY_SIZE(array) sizeof(array) / sizeof(array[0])

#endif