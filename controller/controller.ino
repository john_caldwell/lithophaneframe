#include "src/logging.h"
#include "src/utils.h"
#include "src/animations/animations.h"

#define LED_PIN    6
#define GROUP_SIZE 6
#define GROUPS     4
#define LED_COUNT GROUP_SIZE * GROUPS

Adafruit_NeoPixel pixels(LED_COUNT, LED_PIN, NEO_GRB + NEO_KHZ400);

Color rainbow[] = {
  0xFF0000,
  0xFFA500,
  0xFFFF00,
  0x00FF00,
  0x0000FF,
  0x7F00FF
};

Color spotlight[] = {
  0xFFFFFF,
  0x00,
  0x00,
  0x00,
};

Color crossfadeColors[] = {
  0xFFFFFF, // white

  0xFF0000, // red
  0x00FF00, // green
  0x0000FF, // blue

  0xFFFF00, // yellow
  0x00FFFF, // cyan
  0xFF00FF, // magenta

  0xFF69B4, // pink
  0xEE82EE, // violet
  0x580058, // purple

  0xFFA500, // orange
};

Twinkle twinkle(pixels, GROUP_SIZE, 3 SECONDS);
Spinners rainbowSpinners(pixels, GROUP_SIZE, 10, rainbow, ARRAY_SIZE(rainbow));
Spinners spotlightSpinners(pixels, GROUP_SIZE, 20, spotlight, ARRAY_SIZE(spotlight));
Candles candles(pixels, GROUP_SIZE);
Crossfade crossfade(pixels, GROUP_SIZE, crossfadeColors, ARRAY_SIZE(crossfadeColors), 5 SECONDS, .5 SECONDS);

Animation* animations[] = {
  &candles,
  &twinkle,
  &crossfade,
  &spotlightSpinners,
  &rainbowSpinners,
};
int animationCount = sizeof(animations) / sizeof(animations[0]);

byte currentAnimationIndex = 0;
unsigned long lastAnimationChange = 0;
unsigned long animationChangeFrequency = 10 MINUTES;

void setup() {
  SETUP_LOGGER;

  pixels.begin();
  pixels.clear();
  pixels.show();

  lastAnimationChange = millis();
}

void loop() {
  if (millis() - lastAnimationChange > animationChangeFrequency) {
    currentAnimationIndex = (currentAnimationIndex + 1) % animationCount;
    lastAnimationChange = millis();
  }

  const auto currentAnimation = animations[currentAnimationIndex];
  currentAnimation->animate();

  delay(20);
}
